FROM golang:1.15-alpine AS builder

WORKDIR /app

COPY . . 

RUN ["go", "build"]

FROM golang:1.15-alpine

COPY --from=builder /app/scoreboard-backend-cf12 app

COPY ./scpc-contestants.json ./scpc-contestants.json

COPY ./jcpc-contestants.json ./jcpc-contestants.json

COPY ./.env ./.env

EXPOSE 8080

CMD ["./app"]