package contestantdata

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// GetContestantData returns the specified contestant's data
func GetContestantData(token string, contestantJid string) map[string]interface{} {

	url := os.Getenv("JOPHIEL_URL") + "/api/v2/users/" + contestantJid + "/info"

	var bearer = "Bearer " + token

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {

		log.Fatalln(err)

	}

	req.Header.Add("Authorization", bearer)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {

		log.Fatalln(err)

	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {

		log.Fatalln(err)

	}

	var result map[string]interface{}
	json.Unmarshal([]byte(body), &result)

	return result

}
