module gitlab.com/technical-cpc/scoreboard-backend-cf12

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-co-op/gocron v0.3.0
	github.com/joho/godotenv v1.3.0
)
