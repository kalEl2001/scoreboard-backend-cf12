package scpc

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var beginTime int64
var duration int64
var freezeTime int64

// GetScpcScoreboard will get Scoreboard data from Uriel Service and returns it
func GetScpcScoreboard(token string) map[string]interface{} {

	now := (time.Now()).Unix()

	finalResultTime, err := strconv.ParseInt(os.Getenv("FINAL_RESULT_TIME"), 10, 64)
	if err != nil {
		log.Println(err)
	}

	var url string

	if now-freezeTime >= 0 && now < finalResultTime {
		url = os.Getenv("URIEL_URL") + "/api/v2/contests/" + os.Getenv("SCPC_CONTEST_JID") + "/scoreboard?frozen=true"
	} else {
		url = os.Getenv("URIEL_URL") + "/api/v2/contests/" + os.Getenv("SCPC_CONTEST_JID") + "/scoreboard"
	}

	var bearer = "Bearer " + token

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
	}

	req.Header.Add("Authorization", bearer)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}

	var result map[string]interface{}
	json.Unmarshal([]byte(body), &result)

	if result == nil {
		return nil
	}

	result = result["data"].(map[string]interface{})

	return result

}

//GetScpcContestants returns all the SCPC Contestants data
func GetScpcContestants(token string) interface{} {

	scpcContestantsJSON, err := os.Open(os.Getenv("SCPC_CONTESTANTS_FILE"))
	if err != nil {

		log.Println(err)
		log.Fatalln("No contestants data found!")

	}

	defer scpcContestantsJSON.Close()

	byteValue, _ := ioutil.ReadAll(scpcContestantsJSON)

	var scpcContestants interface{}

	json.Unmarshal([]byte(byteValue), &scpcContestants)

	getContestInfo(token)

	return scpcContestants

}

func getContestInfo(token string) {

	url := os.Getenv("URIEL_URL") + "/api/v2/contests/" + os.Getenv("SCPC_CONTEST_JID")

	var bearer = "Bearer " + token

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
	}

	req.Header.Add("Authorization", bearer)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}

	var result map[string]interface{}
	json.Unmarshal([]byte(body), &result)

	beginTime = int64(result["beginTime"].(float64)) / 1000
	duration = int64(result["duration"].(float64)) / 1000
	freezeTime = beginTime + duration - 3600
}
