# CPC CompFest 12 Scoreboard Backend

This project is using Go Programming Language. This project is made to work with [Judgels](https://github.com/ia-toki/judgels) from early 2020.

## Setup

Use the `.env.example` file as your template for the `.env` configuration file. Fill out every variable value.

Fill out the services' URL with domains of each service.

You will need to fill out the `JUDGELS_ADMIN_*` variable with the credential of one of the accounts with the admin's permission. That credential will be required for requesting the bearer token that will be used to make scoreboard requests to the Uriel service.

You will get the contests' JID from the info of the contest in the Judgels website. You may find that information only if you're signed in as an admin.

Use the example of contestants' data file as the template. Rename it into `scpc-contestants.json` and `jcpc-contestants.json.`

Put the contestant's data at the root of the project. Otherwise, modify the Dockerfile so the `COPY` statements on line 13 and 15 points at the contestant's data file. Modify your `SCPC_CONTESTANTS_FILE` and `JCPC_CONTESTANTS_FILE` variables in the .env file to be the same as the directory of the `COPY` statements destination directory relative to the `WORKDIR` of the image.

Run `go get -d ./...` from the project directory to install all of the project's dependencies.

## Development server

Run `go run main.go` for a dev server. Navigate to `http://localhost:8080/[function_address]`.

## Endpoints

| Address             | Function                  |
|---------------------|---------------------------|
| `/jcpc-contestants` | Get JCPC contestants data |
| `/scpc-contestants` | Get SCPC contestants data |
| `/jcpc-scoreboard`  | Get JCPC scoreboard data  |
| `/scpc-scoreboard`  | Get SCPC scoreboard data  |

## Deployment

Build this project using the Dockerfile provided in this project.

Port 8080 will be exposed as you run the Docker image. Map the port 8080 to the port that you desired.

## Author

By Samuel
samuel92(at)ui.ac.id