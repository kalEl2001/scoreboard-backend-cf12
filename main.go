package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-co-op/gocron"
	_ "github.com/joho/godotenv/autoload"

	"gitlab.com/technical-cpc/scoreboard-backend-cf12/jcpc"
	"gitlab.com/technical-cpc/scoreboard-backend-cf12/scpc"
)

var authToken string

var jcpcScoreboard map[string]interface{}
var jcpcContestants interface{}

var scpcScoreboard map[string]interface{}
var scpcContestants interface{}

func main() {

	updateToken()
	getContestants()
	update()

	s := gocron.NewScheduler(time.UTC)

	s.Every(30).Seconds().Do(update)
	s.Every(1).Day().Do(updateToken)

	s.StartAsync()

	r := gin.Default()

	r.GET("/scpc-scoreboard", ScpcScoreboard)
	r.GET("/scpc-contestants", ScpcContestants)
	r.GET("/jcpc-scoreboard", JcpcScoreboard)
	r.GET("/jcpc-contestants", JcpcContestants)

	r.Run()

}

func getAuth() string {

	requestBody, err := json.Marshal(gin.H{
		"usernameOrEmail": os.Getenv("JUDGELS_ADMIN_USERNAME"),
		"password":        os.Getenv("JUDGELS_ADMIN_PASSWORD"),
	})

	if err != nil {

		log.Println(err)

	}

	url := os.Getenv("JOPHIEL_URL") + "/api/v2/session/login"

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(requestBody))
	if err != nil {

		log.Println(err)

	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {

		log.Println(err)

	}

	var result map[string]interface{}
	json.Unmarshal([]byte(body), &result)

	authToken := fmt.Sprintf("%v", result["token"])

	return authToken

}

func update() {

	jcpcScoreboard = jcpc.GetJcpcScoreboard(authToken)
	scpcScoreboard = scpc.GetScpcScoreboard(authToken)

}

func getContestants() {

	jcpcContestants = jcpc.GetJcpcContestants(authToken)
	scpcContestants = scpc.GetScpcContestants(authToken)

}

func updateToken() {

	authToken = getAuth()

}

//ScpcScoreboard response the request with scoreboard data from Uriel
func ScpcScoreboard(c *gin.Context) {

	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")

	resp := scpcScoreboard

	c.JSON(http.StatusOK, resp)

}

//ScpcContestants response the request with SCPC contestants' data
func ScpcContestants(c *gin.Context) {

	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")

	resp := scpcContestants

	c.JSON(http.StatusOK, resp)

}

//JcpcScoreboard response the request with scoreboard data from Uriel
func JcpcScoreboard(c *gin.Context) {

	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")

	resp := jcpcScoreboard

	c.JSON(http.StatusOK, resp)

}

//JcpcContestants response the request with JCPC contestants' data
func JcpcContestants(c *gin.Context) {

	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")

	resp := jcpcContestants

	c.JSON(http.StatusOK, resp)

}
