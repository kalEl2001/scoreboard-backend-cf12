package jcpc

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var beginTime int64
var duration int64
var freezeTime int64

// GetJcpcScoreboard will get Scoreboard data from Uriel Service and returns it
func GetJcpcScoreboard(token string) map[string]interface{} {

	now := (time.Now()).Unix()

	finalResultTime, err := strconv.ParseInt(os.Getenv("FINAL_RESULT_TIME"), 10, 64)
	if err != nil {
		log.Println(err)
	}

	hideScoreboardWhileContest := os.Getenv("HIDE_JCPC_SCOREBOARD_WHILE_CONTEST")

	var url string

	if hideScoreboardWhileContest == "TRUE" {
		if now < (beginTime + duration) {
			return nil
		}
	}

	if now-freezeTime >= 0 && now < finalResultTime {
		url = os.Getenv("URIEL_URL") + "/api/v2/contests/" + os.Getenv("JCPC_CONTEST_JID") + "/scoreboard?frozen=true"
	} else {
		url = os.Getenv("URIEL_URL") + "/api/v2/contests/" + os.Getenv("JCPC_CONTEST_JID") + "/scoreboard"
	}

	var bearer = "Bearer " + token

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
	}

	req.Header.Add("Authorization", bearer)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}

	var result map[string]interface{}
	json.Unmarshal([]byte(body), &result)

	if result == nil {
		return nil
	}

	result = result["data"].(map[string]interface{})

	return result

}

//GetJcpcContestants returns all the JCPC Contestants data
func GetJcpcContestants(token string) interface{} {

	jcpcContestantsJSON, err := os.Open(os.Getenv("JCPC_CONTESTANTS_FILE"))
	if err != nil {

		log.Println(err)
		log.Fatalln("No contestants data found!")

	}

	defer jcpcContestantsJSON.Close()

	byteValue, _ := ioutil.ReadAll(jcpcContestantsJSON)

	var jcpcContestants interface{}

	json.Unmarshal([]byte(byteValue), &jcpcContestants)

	getContestInfo(token)

	return jcpcContestants

}

func getContestInfo(token string) {

	url := os.Getenv("URIEL_URL") + "/api/v2/contests/" + os.Getenv("JCPC_CONTEST_JID")

	var bearer = "Bearer " + token

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
	}

	req.Header.Add("Authorization", bearer)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}

	var result map[string]interface{}
	json.Unmarshal([]byte(body), &result)

	beginTime = int64(result["beginTime"].(float64)) / 1000
	duration = int64(result["duration"].(float64)) / 1000
	freezeTime = beginTime + duration - 3600
}
